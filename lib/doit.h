#ifndef DOIT_H
#define DOIT_H

namespace calc {

    enum OperationEnum {
        PlusEnum, // '+'
        MinusEnum, // '-'
        MultiplyEnum, // '*'
        DivideEnum // '/'
    };

    enum ErrorsEnum {
        NoErrorEnum, //нет ошибки
        DivideByZeroEnum, //деление на 0 (при операции '/')
        IllegalOperationEnum //неизвестная операция
    };

    double DoIt (int TypeWork, double OperandA, double OperandB, int& ErrorCode);
};

#endif // DOIT_H
