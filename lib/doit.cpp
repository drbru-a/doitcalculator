#include "doit.h"
#include <limits>
#include <cmath>

double calc::DoIt (int TypeWork, double OperandA, double OperandB, int& ErrorCode)
{
    ErrorCode = NoErrorEnum;

    switch(TypeWork) {
    case PlusEnum:
        return OperandA + OperandB;
        break;
    case MinusEnum:
        return OperandA - OperandB;
        break;
    case MultiplyEnum:
        return OperandA * OperandB;
        break;
    case DivideEnum:
        if(fabs(OperandB)>std::numeric_limits<double>::epsilon())
            return OperandA / OperandB;
        else
            ErrorCode = DivideByZeroEnum;
        break;
    default:
        ErrorCode = IllegalOperationEnum;
        break;
    }

    return 0.0;
}
