import QtQuick 2.0
import QtQuick.Controls 2.0
import "enterControl.js" as EnterControl

Rectangle {
    id: rectangle
    color: "#d7d7d7"
    border.width: 2
    border.color: "black"

    signal sendRequest(string str, int timeout);

    function onPressedButton(_char) {
        var res = EnterControl.control(_char, textEnter.text)

        if(res.error===0) {
            textEnter.text = res.str
            textWarning.text = ''
        } else if(res.error===1) {
            rectangle.sendRequest(textEnter.text, textField_Timeout.text)
            textEnter.text = ''
            EnterControl.clear()
            textWarning.text = ''
        } else {
            textWarning.text = EnterControl.getErrorMessage(res.error)
            console.log("ERROR: ", res.error)
        }
    }

    function onRecvConsoleMessage(_type, _message) {
        consoleModel.append({type: _type, message: _message})
        listView_Console.currentIndex = consoleModel.count - 1
    }

    function onRecvStartOperation(str) {
        textRequestInWork.text = str
        textStopwatchRequestInWork.clock_ms = 0
        timerStopwatch.start()
    }

    function onRecvStopOperation(str) {
        timerStopwatch.stop()
        textRequestInWork.text = qsTr('нет')
    }

    function onRecvRequestCount(n) {
        textRequestSize.text = n
    }

    Component.onCompleted: {
        button.pressedButton.connect(onPressedButton)
        button1.pressedButton.connect(onPressedButton)
        button2.pressedButton.connect(onPressedButton)
        button3.pressedButton.connect(onPressedButton)
        button4.pressedButton.connect(onPressedButton)
        button5.pressedButton.connect(onPressedButton)
        button6.pressedButton.connect(onPressedButton)
        button7.pressedButton.connect(onPressedButton)
        button8.pressedButton.connect(onPressedButton)
        button9.pressedButton.connect(onPressedButton)
        button10.pressedButton.connect(onPressedButton)
        button11.pressedButton.connect(onPressedButton)
        button12.pressedButton.connect(onPressedButton)
        button13.pressedButton.connect(onPressedButton)
        button14.pressedButton.connect(onPressedButton)
        button15.pressedButton.connect(onPressedButton)
        button16.pressedButton.connect(onPressedButton)

        rectangle.sendRequest.connect(MainWindow.onSendRequest)
        MainWindow.recvConsoleMessage.connect(onRecvConsoleMessage)
        MainWindow.recvStartOperation.connect(onRecvStartOperation)
        MainWindow.recvStopOperation.connect(onRecvStopOperation)
        MainWindow.recvRequestCount.connect(onRecvRequestCount)
    }

    Timer {
        id: timerStopwatch
        interval: 100
        repeat: true
        running: false

        onTriggered: textStopwatchRequestInWork.clock_ms += 100
    }

    ListModel {
        id: consoleModel
    }

    Rectangle {
        id: fieldEnter
        height: 70
        color: "#ffffff"
        radius: 2
        border.width: 3
        border.color: "black"
        anchors.right: parent.right
        anchors.rightMargin: 4
        anchors.left: parent.left
        anchors.leftMargin: 4
        anchors.top: parent.top
        anchors.topMargin: 4

        Text {
            id: textEnter
            x: 8
            y: 12
            width: 616
            height: 46
            text: ""
            font.bold: true
            verticalAlignment: Text.AlignVCenter
            font.family: "Tahoma"
            font.pixelSize: 18
        }

    }

    Rectangle {
        id: fieldButtons
        width: parent.width*0.65
        height: 314
        color: "#ded8d8"
        border.width: 2
        anchors.left: parent.left
        anchors.leftMargin: 4
        anchors.top: fieldEnter.bottom
        anchors.topMargin: 4

        Grid {
            id: grid
            rows: 4
            columns: 4
            spacing: 5
            anchors.rightMargin: 186
            anchors.leftMargin: 10
            anchors.bottomMargin: 84
            anchors.topMargin: 10
            anchors.fill: parent

            SymButton {
                id: button
                code: "7"
            }

            SymButton {
                id: button1
                code: "8"
            }

            SymButton {
                id: button2
                code: "9"
            }

            SymButton {
                id: button3
                code: "/"
            }
            
            SymButton {
                id: button4
                code: "4"
            }

            SymButton {
                id: button5
                code: "5"
            }

            SymButton {
                id: button6
                code: "6"
            }

            SymButton {
                id: button7
                code: "*"
            }
            
            SymButton {
                id: button8
                code: "1"
            }

            SymButton {
                id: button9
                code: "2"
            }

            SymButton {
                id: button10
                code: "3"
            }

            SymButton {
                id: button11
                code: "-"
            }

            SymButton {
                id: button12
            }

            SymButton {
                id: button13
                code: "."
            }

            SymButton {
                id: button14
                code: "="
            }

            SymButton {
                id: button15
                code: "+"
            }
        }

        SymButton {
            id: button16
            x: 236
            y: 10
            code: "<"
        }

        Text {
            id: element1
            x: 236
            y: 155
            width: 172
            height: 15
            text: qsTr("Время выполнения (сек.)")
            font.pixelSize: 12
        }

        TextField {
            id: textField_Timeout
            x: 264
            y: 176
            width: 63
            height: 40
            text: "5"
            font.bold: true
            font.family: "Verdana"
            placeholderText: qsTr("секунды")
            validator: IntValidator { bottom: 0; top: 99; }
        }

        Text {
            id: textWarning
            x: 21
            y: 246
            width: 374
            height: 42
            color: "#cc0808"
            text: qsTr("Warning")
            font.bold: true
            font.family: "Arial"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 16
        }
    }

    Rectangle {
        id: rectangle1
        height: 314
        color: "#ded8d8"
        anchors.left: fieldButtons.right
        anchors.leftMargin: 4
        anchors.right: parent.right
        anchors.rightMargin: 4
        anchors.top: fieldEnter.bottom
        anchors.topMargin: 4
        border.width: 2

        Text {
            id: element
            x: 8
            y: 8
            width: 132
            height: 15
            text: "Очередь запросов:"
            font.pixelSize: 12
        }

        Text {
            id: element2
            x: 6
            y: 124
            width: 112
            height: 15
            text: qsTr("Очередь ответов:")
            font.pixelSize: 12
        }

        Text {
            id: labelRequestSize
            x: 8
            y: 29
            width: 47
            height: 15
            text: qsTr("размер")
            font.pixelSize: 12
        }

        Text {
            id: labelResultSize
            x: 8
            y: 145
            width: 49
            height: 15
            text: qsTr("размер")
            font.pixelSize: 12
        }

        Text {
            id: textRequestSize
            y: 29
            width: 46
            height: 15
            text: qsTr("0")
            anchors.left: labelRequestSize.right
            anchors.leftMargin: 8
            font.pixelSize: 12
        }

        Text {
            id: textResultSize
            y: 145
            text: qsTr("0")
            anchors.left: labelResultSize.right
            anchors.leftMargin: 8
            font.pixelSize: 12
        }

        Text {
            id: element3
            x: 8
            y: 50
            width: 116
            height: 15
            text: "Запрос в обработке:"
            font.pixelSize: 12
        }

        Text {
            id: textRequestInWork
            x: 8
            y: 71
            width: 188
            height: 15
            text: qsTr("нет")
            font.pixelSize: 12
        }

        Text {
            id: textStopwatchRequestInWork
            y: 92
            width: 82
            height: 15
            text: (clock_ms/1000.0).toFixed(1)
            property int clock_ms: 0
            anchors.left: labelStopwatch.right
            anchors.leftMargin: 8
            font.pixelSize:12
        }

        Text {
            id: labelStopwatch
            x: 6
            y: 92
            width: 79
            height: 15
            text: "Секундомер"
            font.pixelSize: 12
        }
    }

    Rectangle {
        id: fieldConsole
        color: "#403a3a"
        border.width: 2
        border.color: "#37a639"
        anchors.right: parent.right
        anchors.rightMargin: 4
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 4
        anchors.left: parent.left
        anchors.leftMargin: 4
        anchors.top: fieldButtons.bottom
        anchors.topMargin: 4

        ListView {
            id: listView_Console
            anchors.fill: parent
            flickableDirection: Flickable.VerticalFlick
            boundsBehavior: Flickable.StopAtBounds
            clip: true

            delegate: Item {
                x: 2
                width: listView_Console.width - 4
                height: 20

                Text {
                    text: '> ' + message
                    anchors.verticalCenter: parent.verticalCenter
                    font.bold: true
                    color: (type==='request')?'#9ffd8f':(type==='result'?'#9f8fff':'red')
                }
            }
            model: consoleModel

            add: Transition {
                NumberAnimation { property: "opacity"; from: 0; to: 1.0; duration: 400 }
                NumberAnimation { property: "scale"; from: 0; to: 1.0; duration: 400 }
            }

            displaced: Transition {
                NumberAnimation { properties: "x,y"; duration: 400; easing.type: Easing.OutBounce }
            }

            ScrollBar.vertical: ScrollBar {
                active: true
                policy: ScrollBar.AlwaysOn
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:2;anchors_x:8;anchors_y:80}D{i:7;anchors_height:74;anchors_width:193;anchors_x:439;anchors_y:78}
D{i:5;anchors_x:8;anchors_y:80}D{i:4;anchors_height:74;anchors_width:193;anchors_x:439;anchors_y:78}
D{i:32;anchors_x:62}D{i:33;anchors_x:63}D{i:36;anchors_x:82}D{i:27;anchors_height:74;anchors_width:624;anchors_x:8;anchors_y:398}
D{i:39;anchors_height:74;anchors_width:624;anchors_x:8;anchors_y:398}
}
##^##*/
