
const operatorSymbols = ['+', '-', '*', '/']
const numberSymbols = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
var operator_entered = '' //введенный оператор
var is_point = false //признак того что введен символ '.' в числе


/*!
 * функция контроля ввода
 * char - входной символ
 * text - уже введенная строк
 *
 * возвращает {error: <код ожибки>, str: <новая строка>}
 * если error!==0 то str не учитывется и равна ''
 * коды ошибок:
 *   2 - после введенного символа '=' признак того что строка введена не полностью
 *   1 - после введенного символа '=' признак того что строка введена полностью
 *   0 - нет ошибки
 *   -1 - ввод символа '.' в данном месте не допустимо
 *   -2 - ввод символа оператора в данном месте не допустимо
 *   -3 - ожидается ввод символа '.' после ввода 0 первым символом в начале строки или после оператора
 *   -4 - нечего стирать, когда приходит символ < при пустой введенной строке
 */
function control(char, text) {
    var last_char = (text.length>0) ? text[text.length - 1] : ''

    if(char==='<') {
        if(text.length===0)
            return {error: -4, str: ''}
        else if(operatorSymbols.indexOf(last_char)!==-1) {
            operator_entered = ''
            is_point = (text.indexOf('.')!==-1)
        } else if(last_char==='.')
            is_point = false
        return {error: 0, str: text.slice(0, -1)}
    } else if(char==='.') {
        if(text.length===0 || last_char==='.' || operatorSymbols.indexOf(last_char)!==-1 || is_point)
           return {error: -1, str: ''}
        else
            is_point = true
    } else if(operatorSymbols.indexOf(char)!==-1) {
        if(text.length===0 || last_char==='.' || operatorSymbols.indexOf(last_char)!==-1 || operator_entered!=='')
           return {error: -2, str: ''}
        else {
            operator_entered = char
            is_point = false
        }
    } else if(numberSymbols.indexOf(char)!==-1) {
        if(last_char==='0' && (text.length===1 || operatorSymbols.indexOf(text[text.length-2])!==-1) )
            return {error: -3, str: ''}
    } else if(char==='=') {
        if(operator_entered==='' || last_char===operator_entered || last_char==='.')
            return {error: 2, str: ''}
        else
            return {error: 1, str: ''}
    }

    return {error: 0, str: text+char}
}

function clear() {
    operator_entered = ''
    is_point = false
}

function getErrorMessage(code) {
    switch(code) {
    case 2: return qsTr('выражение не завершено')
    case -1: return qsTr("здесь нельзя вводить '.'")
    case -2: return qsTr('здесь нельзя вводить оператор')
    case -3: return qsTr("ожидается ввод символа '.'")
    case -4: return qsTr('нечего стирать')
    }

    return qsTr('нет ошибки')
}
