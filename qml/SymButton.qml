import QtQuick 2.0

Rectangle {
    id: button
    property string code: "0"
    border.color: 'black'
    width: 50
    height: 50

    signal pressedButton(string str)

    function getText(str) {
        if(str==="*")
            return "\u00d7"
        else if(str==="/")
            return "\u00f7"
        else
            return str
    }

    Text {
        id: element
        text: getText(button.code)
        font.bold: true
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        anchors.rightMargin: 4
        anchors.leftMargin: 4
        anchors.bottomMargin: 4
        anchors.topMargin: 4
        anchors.fill: parent
        font.pixelSize: 18
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent

        Connections {
            target: mouseArea
            onPressed: {
                button.color = 'gray';
                button.pressedButton(button.code)
            }
        }

        Connections {
            target: mouseArea
            onReleased: { button.color = 'white' }
        }
    }


}

/*##^##
Designer {
    D{i:1;anchors_height:13;anchors_width:22;anchors_x:13;anchors_y:18}D{i:2;anchors_height:100;anchors_width:100}
}
##^##*/
