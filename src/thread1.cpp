#include <QMap>
#include <thread>
#include "thread1.h"
#include "doit.h"

#include <QDebug>

Thread1::Thread1(QObject *parent)
    : QThread(parent)
{
}

void Thread1::setQueues(QueueRequests *_request, QueueResults *_result)
{
    m_request = _request;
    m_result = _result;
}

void Thread1::stop()
{
    m_stop = true;
    QThread::usleep(10);
    m_request->wakeAllWaiting();
}

void Thread1::run()
{
    while(!m_stop) {
        request r;
        bool ok = m_request->wait_and_pop(r);
        if(!ok) continue;

        QString str = printOperation(r);
        emit startOperation(str);
        double res;
        int error;
        auto tdoit = std::thread([&](int _operation, double _a, double _b, int &_error) {
            res = calc::DoIt(_operation, _a, _b, _error);
        }, r.operation, r.a, r.b, std::ref(error) );
        tdoit.join();
        int n = r.timeout*10;
        for(int i=0; i<n; ++i) {
            this->msleep(100);
            if(m_stop) break;
        }
        emit stopOperation(str);

        m_result->push(result(res, error));
        emit resultReadyRead();
    }

    //qDebug()<<Q_FUNC_INFO<<"Thread stop";
}

QString Thread1::printOperation(const request &_r)
{
    static QMap<int, QString> actions = {{calc::PlusEnum, "+"}, {calc::MinusEnum, "-"}, {calc::MultiplyEnum, "*"}, {calc::DivideEnum, "/"}};
    return QString("%1 %2 %3").arg(_r.a).arg(actions.value(_r.operation)).arg(_r.b);
}
