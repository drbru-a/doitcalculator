#ifndef RESULT_H
#define RESULT_H


/*!
 * \brief Результат операции
 */
class result
{
public:
    result();
    result(double _value, int _error);
    double value; //значение
    int error; //ошибка
};

#endif // RESULT_H
