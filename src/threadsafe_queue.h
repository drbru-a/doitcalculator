#ifndef TH_QUEUE_H
#define TH_QUEUE_H

#include <QMutex>
#include <QWaitCondition>
#include <queue>

/*!
 *  Шаблон потокобезопастной очереди
 */
template <typename T>
class Threadsafe_queue
{
public:
    Threadsafe_queue() {}
    Threadsafe_queue(Threadsafe_queue const &other) {
        QMutexLocker locker(&m_lock);
        m_queue = other.m_queue;
    }

    void push(T new_value) {
        QMutexLocker locker(&m_lock);
        m_queue.push(new_value);
        m_cond.wakeOne();
    }

    bool wait_and_pop(T& value) {
        QMutexLocker locker(&m_lock);
        if(m_queue.empty()) {
            m_cond.wait(&m_lock);
        }

        if(m_queue.empty())
            return false;

        value = m_queue.front();
        m_queue.pop();

        return true;
    }

    bool try_pop(T& value) {
        QMutexLocker locker(&m_lock);
        if(m_queue.empty())
            return false;
        value = m_queue.front();
        m_queue.pop();
        return true;
    }

    int size() const {
        QMutexLocker locker(&m_lock);
        return m_queue.size();
    }

    bool empty() const {
        QMutexLocker locker(&m_lock);
        return m_queue.empty();
    }

    /*!
     * \brief wakeAllWaiting
     * используется чтобы разблокировать ожидание чтобы завершить поток
     */
    void wakeAllWaiting() {
        QMutexLocker locker(&m_lock);
        m_cond.wakeAll();
    }
private:
    mutable QMutex m_lock;
    QWaitCondition m_cond;
    std::queue<T> m_queue;
};

#endif // TH_QUEUE_H
