#ifndef WORK_QUEUES_H
#define WORK_QUEUES_H

#include "threadsafe_queue.h"
#include "request.h"
#include "result.h"

using QueueRequests = Threadsafe_queue<request>;
using QueueResults = Threadsafe_queue<result>;

#endif // WORK_QUEUES_H
