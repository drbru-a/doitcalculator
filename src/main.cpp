#include <QApplication>
#include "mainwindow.h"
#include "thread1.h"
#include "work_queues.h"
#include "doit.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QueueRequests requests;
    QueueResults results;

    MainWindow w;
    w.setQueues(&requests, &results);
    w.show();

    Thread1 thread1;
    thread1.setQueues(&requests, &results);

    w.connect(&thread1, &Thread1::startOperation, &w, &MainWindow::onStartOperation );
    w.connect(&thread1, &Thread1::stopOperation, &w, &MainWindow::onStopOperation );
    w.connect(&thread1, &Thread1::resultReadyRead, &w, &MainWindow::onResultReadyRead );

    thread1.start();

    int result = a.exec();

    thread1.stop();
    thread1.wait();

    return result;
}
