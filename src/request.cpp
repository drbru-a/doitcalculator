#include "request.h"
#include "doit.h"

request::request()
    : operation{calc::PlusEnum}, a{0.0}, b{0.0}, timeout{0}
{
}

request::request(int _operation, double _a, double _b, int _timeout)
    : operation{_operation}, a{_a}, b{_b}, timeout{_timeout}
{
}
