#ifndef THREAD1_H
#define THREAD1_H

#include <QThread>
#include "work_queues.h"

class Thread1 : public QThread
{
    Q_OBJECT
signals:
    void startOperation(QString str);
    void stopOperation(QString str);
    void resultReadyRead();

public:
    Thread1(QObject *parent=nullptr);

    void setQueues(QueueRequests *_request, QueueResults *_result);

    void stop();

protected:
    void run() override;

private:
    QueueRequests *m_request{nullptr};
    QueueResults *m_result{nullptr};
    bool m_stop{false};

    QString printOperation(request const& _r);
};

#endif // THREAD1_H
