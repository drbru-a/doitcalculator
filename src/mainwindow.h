#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "work_queues.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

signals:
    void recvConsoleMessage(QString type, QString message);
    void recvStartOperation(QString op);
    void recvStopOperation();
    void recvRequestCount(int n);

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void setQueues(QueueRequests *_request, QueueResults *_result);

    Q_SLOT void onStartOperation(QString _str);
    Q_SLOT void onStopOperation(QString _str);
    Q_SLOT void onSendRequest(QString _str, int _timeout);
    Q_SLOT void onResultReadyRead();

private:
    Ui::MainWindow *ui;
    QueueRequests *m_request{nullptr};
    QueueResults *m_result{nullptr};
};
#endif // MAINWINDOW_H
