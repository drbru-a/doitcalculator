#include <QQmlContext>
#include <QQuickWidget>
#include <QJsonDocument>
#include <QJsonValue>
#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "doit.h"

#include <QDebug>
#include <QJsonObject>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QFile fsettings("settings.json");
    if(fsettings.open(QIODevice::ReadOnly)) {
        QJsonParseError jerror;
        auto jdoc = QJsonDocument::fromJson(fsettings.readAll(), &jerror);

        if(jerror.error==QJsonParseError::NoError) {
            auto j_x = jdoc["x"];
            auto j_y = jdoc["y"];
            auto j_width = jdoc["width"];
            auto j_height = jdoc["height"];

            if(j_x.isDouble() && j_y.isDouble()) {
                this->move(j_x.toInt(), j_y.toInt());
            }

            if(j_width.isDouble() && j_height.isDouble()) {
                this->resize(j_width.toInt(), j_height.toInt());
            }
        }

        fsettings.close();
    }

    QQmlContext *context = ui->quickWidget->rootContext();
    context->setContextProperty("MainWindow", this);

    QUrl source("qrc:/qml/MainForm.qml");
    ui->quickWidget->setSource(source);
}

MainWindow::~MainWindow()
{
    QFile fsettings("settings.json");
    if(fsettings.open(QIODevice::WriteOnly)) {
        QJsonObject jobject;

        jobject["x"] = this->x();
        jobject["y"] = this->y();
        jobject["width"] = this->width();
        jobject["height"] = this->height();

        QJsonDocument jdoc(jobject);
        fsettings.write(jdoc.toJson());

        fsettings.close();
    }

    delete ui;
}

void MainWindow::setQueues(QueueRequests *_request, QueueResults *_result)
{
    m_request = _request;
    m_result = _result;
}

void MainWindow::onStartOperation(QString _str)
{
    //qDebug()<<Q_FUNC_INFO;
    emit recvStartOperation(_str);
    emit recvRequestCount(m_request->size());
}

void MainWindow::onStopOperation(QString _str)
{
    //qDebug()<<Q_FUNC_INFO<<_str;
    emit recvStopOperation();
}

void MainWindow::onSendRequest(QString _str, int _timeout)
{
    static QMap<QString, int> id_op = {{"+", calc::PlusEnum}
                                       , {"-", calc::MinusEnum}
                                       , {"*", calc::MultiplyEnum}
                                       , {"/", calc::DivideEnum} };

    QStringList values = _str.split(QRegExp("[-/+/*//]"));
    if(values.size()==2) {
        QString str_op = _str;
        str_op.remove(values[0]).remove(values[1]);
        int op = id_op.value(str_op);
        double a, b;
        bool errorA, errorB;
        a = values[0].toDouble(&errorA);
        b = values[1].toDouble(&errorB);
        if(errorA && errorB) {
            m_request->push(request(op, a, b, _timeout));
            emit recvRequestCount(m_request->size());
            emit recvConsoleMessage("request", _str);
            qDebug()<<"request"<<_str;
        }
    }
    qDebug()<<Q_FUNC_INFO<<_str<<_timeout<<values;
}

void MainWindow::onResultReadyRead()
{
    if(!m_result->empty()) {
        result res;
        m_result->try_pop(res);
        if(res.error==calc::NoErrorEnum)
            emit recvConsoleMessage("result", QString::number(res.value));
        else if(res.error==calc::DivideByZeroEnum)
            emit recvConsoleMessage("error", tr("деление на 0"));
    }
}

