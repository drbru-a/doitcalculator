#ifndef REQUEST_H
#define REQUEST_H


/*!
 * \brief Запрос
 */
class request
{
public:
    request();
    request(int _operation, double _a, double _b, int _timeout);
    int operation;
    double a;
    double b;
    int timeout;
};

#endif // REQUEST_H
